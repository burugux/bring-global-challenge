## Running the application
1. Clone via https
`https://gitlab.com/burugux/bring-global-challenge.git`

2. If use are using Maven, run the application with
```console
mvn spring-boot:run
```
## Generating JARs
 * Using Maven
 `mvn clean install ` or `mvn clean package`
 
 ## Running the JAR
 * JARs and other compiled assets can be found in the resources folder
 ```console
 java -jar target/demo-0.0.1-SNAPSHOT.jar
 ```

## Accessing the swagger UI locally
http://localhost:8085/swagger-ui.html

## Further Help
Refer to [HELP.md](HELP.md) for getting started with spring MVC and maven.