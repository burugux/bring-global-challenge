package com.bring.global.demo.controllers;

import com.bring.global.demo.models.Transaction;
import com.bring.global.demo.models.TransactionList;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

import static org.slf4j.LoggerFactory.getLogger;

@RestController
@RequestMapping("/v1/transactions")
public class TransactionsController {
    private static final Logger logger = getLogger(TransactionsController.class);
    private final String url = "https://apisandbox.openbankproject.com/obp/v1.2.1/banks/rbs/accounts/savings-kids-john/public/transactions";

    @ApiOperation(value = "Get a list of transactions", notes = "Get a list of transactions from an account")
    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public TransactionList listTransactions() {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.getForObject(
                url,
                TransactionList.class);
    }

    @ApiOperation(value = "Filter transactions", notes = "Filter transactions from an account by transaction type")
    @GetMapping(value = "/types", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Transaction> filterTransactions(@RequestParam String type) {
        RestTemplate restTemplate = new RestTemplate();
        List<Transaction> transactionsByType;
        TransactionList response = restTemplate.getForObject(
                url,
                TransactionList.class);
        assert response != null;
        transactionsByType = response.getTransactions()
                .stream()
                .filter(transaction -> transaction.getDetails()
                        .getType().equalsIgnoreCase(type))
                .collect(Collectors.toList());
        return transactionsByType;
    }
}
