
package com.bring.global.demo.models;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Metadata {

    @Expose
    private List<Comment> comments;
    @SerializedName("corporate_location")
    private CorporateLocation corporateLocation;
    @SerializedName("image_URL")
    private String imageURL;
    @Expose
    private List<Image> images;
    @SerializedName("more_info")
    private String moreInfo;
    @Expose
    private String narrative;
    @SerializedName("open_corporates_URL")
    private String openCorporatesURL;
    @SerializedName("physical_location")
    private PhysicalLocation physicalLocation;
    @SerializedName("private_alias")
    private String privateAlias;
    @SerializedName("public_alias")
    private String publicAlias;
    @Expose
    private List<Tag> tags;
    @SerializedName("URL")
    private String uRL;
    @Expose
    private Where where;

    public List<Comment> getComments() {
        return comments;
    }

    public CorporateLocation getCorporateLocation() {
        return corporateLocation;
    }

    public String getImageURL() {
        return imageURL;
    }

    public List<Image> getImages() {
        return images;
    }

    public String getMoreInfo() {
        return moreInfo;
    }

    public String getNarrative() {
        return narrative;
    }

    public String getOpenCorporatesURL() {
        return openCorporatesURL;
    }

    public PhysicalLocation getPhysicalLocation() {
        return physicalLocation;
    }

    public String getPrivateAlias() {
        return privateAlias;
    }

    public String getPublicAlias() {
        return publicAlias;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public String getURL() {
        return uRL;
    }

    public Where getWhere() {
        return where;
    }

    public static class Builder {

        private List<Comment> comments;
        private CorporateLocation corporateLocation;
        private String imageURL;
        private List<Image> images;
        private String moreInfo;
        private String narrative;
        private String openCorporatesURL;
        private PhysicalLocation physicalLocation;
        private String privateAlias;
        private String publicAlias;
        private List<Tag> tags;
        private String uRL;
        private Where where;

        public Metadata.Builder withComments(List<Comment> comments) {
            this.comments = comments;
            return this;
        }

        public Metadata.Builder withCorporateLocation(CorporateLocation corporateLocation) {
            this.corporateLocation = corporateLocation;
            return this;
        }

        public Metadata.Builder withImageURL(String imageURL) {
            this.imageURL = imageURL;
            return this;
        }

        public Metadata.Builder withImages(List<Image> images) {
            this.images = images;
            return this;
        }

        public Metadata.Builder withMoreInfo(String moreInfo) {
            this.moreInfo = moreInfo;
            return this;
        }

        public Metadata.Builder withNarrative(String narrative) {
            this.narrative = narrative;
            return this;
        }

        public Metadata.Builder withOpenCorporatesURL(String openCorporatesURL) {
            this.openCorporatesURL = openCorporatesURL;
            return this;
        }

        public Metadata.Builder withPhysicalLocation(PhysicalLocation physicalLocation) {
            this.physicalLocation = physicalLocation;
            return this;
        }

        public Metadata.Builder withPrivateAlias(String privateAlias) {
            this.privateAlias = privateAlias;
            return this;
        }

        public Metadata.Builder withPublicAlias(String publicAlias) {
            this.publicAlias = publicAlias;
            return this;
        }

        public Metadata.Builder withTags(List<Tag> tags) {
            this.tags = tags;
            return this;
        }

        public Metadata.Builder withURL(String uRL) {
            this.uRL = uRL;
            return this;
        }

        public Metadata.Builder withWhere(Where where) {
            this.where = where;
            return this;
        }

        public Metadata build() {
            Metadata metadata = new Metadata();
            metadata.comments = comments;
            metadata.corporateLocation = corporateLocation;
            metadata.imageURL = imageURL;
            metadata.images = images;
            metadata.moreInfo = moreInfo;
            metadata.narrative = narrative;
            metadata.openCorporatesURL = openCorporatesURL;
            metadata.physicalLocation = physicalLocation;
            metadata.privateAlias = privateAlias;
            metadata.publicAlias = publicAlias;
            metadata.tags = tags;
            metadata.uRL = uRL;
            metadata.where = where;
            return metadata;
        }

    }

}
