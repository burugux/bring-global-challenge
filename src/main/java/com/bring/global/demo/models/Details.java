
package com.bring.global.demo.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Details {

    @Expose
    private String completed;
    @Expose
    private String description;
    @SerializedName("new_balance")
    private NewBalance newBalance;
    @Expose
    private String posted;
    @Expose
    private String type;
    @Expose
    private Value value;

    public String getCompleted() {
        return completed;
    }

    public String getDescription() {
        return description;
    }

    public NewBalance getNewBalance() {
        return newBalance;
    }

    public String getPosted() {
        return posted;
    }

    public String getType() {
        return type;
    }

    public Value getValue() {
        return value;
    }

    public static class Builder {

        private String completed;
        private String description;
        private NewBalance newBalance;
        private String posted;
        private String type;
        private Value value;

        public Details.Builder withCompleted(String completed) {
            this.completed = completed;
            return this;
        }

        public Details.Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Details.Builder withNewBalance(NewBalance newBalance) {
            this.newBalance = newBalance;
            return this;
        }

        public Details.Builder withPosted(String posted) {
            this.posted = posted;
            return this;
        }

        public Details.Builder withType(String type) {
            this.type = type;
            return this;
        }

        public Details.Builder withValue(Value value) {
            this.value = value;
            return this;
        }

        public Details build() {
            Details details = new Details();
            details.completed = completed;
            details.description = description;
            details.newBalance = newBalance;
            details.posted = posted;
            details.type = type;
            details.value = value;
            return details;
        }

    }

}
