
package com.bring.global.demo.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class OtherAccount {

    @Expose
    private Bank bank;
    @Expose
    private Holder holder;
    @SerializedName("IBAN")
    private String iBAN;
    @Expose
    private String id;
    @Expose
    private String kind;
    @Expose
    private Metadata metadata;
    @Expose
    private String number;
    @SerializedName("swift_bic")
    private String swiftBic;

    public Bank getBank() {
        return bank;
    }

    public Holder getHolder() {
        return holder;
    }

    public String getIBAN() {
        return iBAN;
    }

    public String getId() {
        return id;
    }

    public String getKind() {
        return kind;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public String getNumber() {
        return number;
    }

    public String getSwiftBic() {
        return swiftBic;
    }

    public static class Builder {

        private Bank bank;
        private Holder holder;
        private String iBAN;
        private String id;
        private String kind;
        private Metadata metadata;
        private String number;
        private String swiftBic;

        public OtherAccount.Builder withBank(Bank bank) {
            this.bank = bank;
            return this;
        }

        public OtherAccount.Builder withHolder(Holder holder) {
            this.holder = holder;
            return this;
        }

        public OtherAccount.Builder withIBAN(String iBAN) {
            this.iBAN = iBAN;
            return this;
        }

        public OtherAccount.Builder withId(String id) {
            this.id = id;
            return this;
        }

        public OtherAccount.Builder withKind(String kind) {
            this.kind = kind;
            return this;
        }

        public OtherAccount.Builder withMetadata(Metadata metadata) {
            this.metadata = metadata;
            return this;
        }

        public OtherAccount.Builder withNumber(String number) {
            this.number = number;
            return this;
        }

        public OtherAccount.Builder withSwiftBic(String swiftBic) {
            this.swiftBic = swiftBic;
            return this;
        }

        public OtherAccount build() {
            OtherAccount otherAccount = new OtherAccount();
            otherAccount.bank = bank;
            otherAccount.holder = holder;
            otherAccount.iBAN = iBAN;
            otherAccount.id = id;
            otherAccount.kind = kind;
            otherAccount.metadata = metadata;
            otherAccount.number = number;
            otherAccount.swiftBic = swiftBic;
            return otherAccount;
        }

    }

}
