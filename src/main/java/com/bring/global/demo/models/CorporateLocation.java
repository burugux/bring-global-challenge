
package com.bring.global.demo.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CorporateLocation {

    @Expose
    private String date;
    @Expose
    private Double latitude;
    @Expose
    private Double longitude;
    @Expose
    private User user;

    public String getDate() {
        return date;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public User getUser() {
        return user;
    }

    public static class Builder {

        private String date;
        private Double latitude;
        private Double longitude;
        private User user;

        public CorporateLocation.Builder withDate(String date) {
            this.date = date;
            return this;
        }

        public CorporateLocation.Builder withLatitude(Double latitude) {
            this.latitude = latitude;
            return this;
        }

        public CorporateLocation.Builder withLongitude(Double longitude) {
            this.longitude = longitude;
            return this;
        }

        public CorporateLocation.Builder withUser(User user) {
            this.user = user;
            return this;
        }

        public CorporateLocation build() {
            CorporateLocation corporateLocation = new CorporateLocation();
            corporateLocation.date = date;
            corporateLocation.latitude = latitude;
            corporateLocation.longitude = longitude;
            corporateLocation.user = user;
            return corporateLocation;
        }

    }

}
