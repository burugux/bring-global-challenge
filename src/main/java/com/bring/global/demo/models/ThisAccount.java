
package com.bring.global.demo.models;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ThisAccount {

    @Expose
    private Bank bank;
    @Expose
    private List<Holder> holders;
    @SerializedName("IBAN")
    private String iBAN;
    @Expose
    private String id;
    @Expose
    private String kind;
    @Expose
    private String number;
    @SerializedName("swift_bic")
    private String swiftBic;

    public Bank getBank() {
        return bank;
    }

    public List<Holder> getHolders() {
        return holders;
    }

    public String getIBAN() {
        return iBAN;
    }

    public String getId() {
        return id;
    }

    public String getKind() {
        return kind;
    }

    public String getNumber() {
        return number;
    }

    public String getSwiftBic() {
        return swiftBic;
    }

    public static class Builder {

        private Bank bank;
        private List<Holder> holders;
        private String iBAN;
        private String id;
        private String kind;
        private String number;
        private String swiftBic;

        public ThisAccount.Builder withBank(Bank bank) {
            this.bank = bank;
            return this;
        }

        public ThisAccount.Builder withHolders(List<Holder> holders) {
            this.holders = holders;
            return this;
        }

        public ThisAccount.Builder withIBAN(String iBAN) {
            this.iBAN = iBAN;
            return this;
        }

        public ThisAccount.Builder withId(String id) {
            this.id = id;
            return this;
        }

        public ThisAccount.Builder withKind(String kind) {
            this.kind = kind;
            return this;
        }

        public ThisAccount.Builder withNumber(String number) {
            this.number = number;
            return this;
        }

        public ThisAccount.Builder withSwiftBic(String swiftBic) {
            this.swiftBic = swiftBic;
            return this;
        }

        public ThisAccount build() {
            ThisAccount thisAccount = new ThisAccount();
            thisAccount.bank = bank;
            thisAccount.holders = holders;
            thisAccount.iBAN = iBAN;
            thisAccount.id = id;
            thisAccount.kind = kind;
            thisAccount.number = number;
            thisAccount.swiftBic = swiftBic;
            return thisAccount;
        }

    }

}
