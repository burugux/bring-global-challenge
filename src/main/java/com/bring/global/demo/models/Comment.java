
package com.bring.global.demo.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Comment {

    @Expose
    private String date;
    @Expose
    private String id;
    @Expose
    private User user;
    @Expose
    private String value;

    public String getDate() {
        return date;
    }

    public String getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public String getValue() {
        return value;
    }

    public static class Builder {

        private String date;
        private String id;
        private User user;
        private String value;

        public Comment.Builder withDate(String date) {
            this.date = date;
            return this;
        }

        public Comment.Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Comment.Builder withUser(User user) {
            this.user = user;
            return this;
        }

        public Comment.Builder withValue(String value) {
            this.value = value;
            return this;
        }

        public Comment build() {
            Comment comment = new Comment();
            comment.date = date;
            comment.id = id;
            comment.user = user;
            comment.value = value;
            return comment;
        }

    }

}
