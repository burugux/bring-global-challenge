
package com.bring.global.demo.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class PhysicalLocation {

    @Expose
    private String date;
    @Expose
    private Double latitude;
    @Expose
    private Double longitude;
    @Expose
    private User user;

    public String getDate() {
        return date;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public User getUser() {
        return user;
    }

    public static class Builder {

        private String date;
        private Double latitude;
        private Double longitude;
        private User user;

        public PhysicalLocation.Builder withDate(String date) {
            this.date = date;
            return this;
        }

        public PhysicalLocation.Builder withLatitude(Double latitude) {
            this.latitude = latitude;
            return this;
        }

        public PhysicalLocation.Builder withLongitude(Double longitude) {
            this.longitude = longitude;
            return this;
        }

        public PhysicalLocation.Builder withUser(User user) {
            this.user = user;
            return this;
        }

        public PhysicalLocation build() {
            PhysicalLocation physicalLocation = new PhysicalLocation();
            physicalLocation.date = date;
            physicalLocation.latitude = latitude;
            physicalLocation.longitude = longitude;
            physicalLocation.user = user;
            return physicalLocation;
        }

    }

}
