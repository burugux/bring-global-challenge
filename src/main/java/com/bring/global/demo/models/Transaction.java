
package com.bring.global.demo.models;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Transaction {

    @Expose
    private Details details;
    @Expose
    private String id;
    @Expose
    private Metadata metadata;
    @SerializedName("other_account")
    private OtherAccount otherAccount;
    @SerializedName("this_account")
    private ThisAccount thisAccount;
    @Expose
    private List<Transaction> transactions;

    public Details getDetails() {
        return details;
    }

    public String getId() {
        return id;
    }

    public Metadata getMetadata() {
        return metadata;
    }

    public OtherAccount getOtherAccount() {
        return otherAccount;
    }

    public ThisAccount getThisAccount() {
        return thisAccount;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public static class Builder {

        private Details details;
        private String id;
        private Metadata metadata;
        private OtherAccount otherAccount;
        private ThisAccount thisAccount;
        private List<Transaction> transactions;

        public Transaction.Builder withDetails(Details details) {
            this.details = details;
            return this;
        }

        public Transaction.Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Transaction.Builder withMetadata(Metadata metadata) {
            this.metadata = metadata;
            return this;
        }

        public Transaction.Builder withOtherAccount(OtherAccount otherAccount) {
            this.otherAccount = otherAccount;
            return this;
        }

        public Transaction.Builder withThisAccount(ThisAccount thisAccount) {
            this.thisAccount = thisAccount;
            return this;
        }

        public Transaction.Builder withTransactions(List<Transaction> transactions) {
            this.transactions = transactions;
            return this;
        }

        public Transaction build() {
            Transaction transaction = new Transaction();
            transaction.details = details;
            transaction.id = id;
            transaction.metadata = metadata;
            transaction.otherAccount = otherAccount;
            transaction.thisAccount = thisAccount;
            transaction.transactions = transactions;
            return transaction;
        }

    }

}
