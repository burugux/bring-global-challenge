
package com.bring.global.demo.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Holder {

    @SerializedName("is_alias")
    private String isAlias;
    @Expose
    private String name;

    public String getIsAlias() {
        return isAlias;
    }

    public String getName() {
        return name;
    }

    public static class Builder {

        private String isAlias;
        private String name;

        public Holder.Builder withIsAlias(String isAlias) {
            this.isAlias = isAlias;
            return this;
        }

        public Holder.Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Holder build() {
            Holder holder = new Holder();
            holder.isAlias = isAlias;
            holder.name = name;
            return holder;
        }

    }

}
