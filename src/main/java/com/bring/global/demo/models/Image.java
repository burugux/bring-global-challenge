
package com.bring.global.demo.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Image {

    @Expose
    private String id;
    @Expose
    private String label;
    @SerializedName("URL")
    private String uRL;

    public String getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public String getURL() {
        return uRL;
    }

    public static class Builder {

        private String id;
        private String label;
        private String uRL;

        public Image.Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Image.Builder withLabel(String label) {
            this.label = label;
            return this;
        }

        public Image.Builder withURL(String uRL) {
            this.uRL = uRL;
            return this;
        }

        public Image build() {
            Image image = new Image();
            image.id = id;
            image.label = label;
            image.uRL = uRL;
            return image;
        }

    }

}
