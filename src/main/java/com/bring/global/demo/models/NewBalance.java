
package com.bring.global.demo.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class NewBalance {

    @Expose
    private String amount;
    @Expose
    private String currency;

    public String getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public static class Builder {

        private String amount;
        private String currency;

        public NewBalance.Builder withAmount(String amount) {
            this.amount = amount;
            return this;
        }

        public NewBalance.Builder withCurrency(String currency) {
            this.currency = currency;
            return this;
        }

        public NewBalance build() {
            NewBalance newBalance = new NewBalance();
            newBalance.amount = amount;
            newBalance.currency = currency;
            return newBalance;
        }

    }

}
