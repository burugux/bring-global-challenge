
package com.bring.global.demo.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Value {

    @Expose
    private String amount;
    @Expose
    private String currency;

    public String getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public static class Builder {

        private String amount;
        private String currency;

        public Value.Builder withAmount(String amount) {
            this.amount = amount;
            return this;
        }

        public Value.Builder withCurrency(String currency) {
            this.currency = currency;
            return this;
        }

        public Value build() {
            Value value = new Value();
            value.amount = amount;
            value.currency = currency;
            return value;
        }

    }

}
