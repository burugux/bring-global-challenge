
package com.bring.global.demo.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class User {

    @SerializedName("display_name")
    private String displayName;
    @Expose
    private String id;
    @Expose
    private String provider;

    public String getDisplayName() {
        return displayName;
    }

    public String getId() {
        return id;
    }

    public String getProvider() {
        return provider;
    }

    public static class Builder {

        private String displayName;
        private String id;
        private String provider;

        public User.Builder withDisplayName(String displayName) {
            this.displayName = displayName;
            return this;
        }

        public User.Builder withId(String id) {
            this.id = id;
            return this;
        }

        public User.Builder withProvider(String provider) {
            this.provider = provider;
            return this;
        }

        public User build() {
            User user = new User();
            user.displayName = displayName;
            user.id = id;
            user.provider = provider;
            return user;
        }

    }

}
