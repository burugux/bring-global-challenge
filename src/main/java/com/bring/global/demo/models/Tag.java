
package com.bring.global.demo.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Tag {

    @Expose
    private String date;
    @Expose
    private String id;
    @Expose
    private User user;
    @Expose
    private String value;

    public String getDate() {
        return date;
    }

    public String getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public String getValue() {
        return value;
    }

    public static class Builder {

        private String date;
        private String id;
        private User user;
        private String value;

        public Tag.Builder withDate(String date) {
            this.date = date;
            return this;
        }

        public Tag.Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Tag.Builder withUser(User user) {
            this.user = user;
            return this;
        }

        public Tag.Builder withValue(String value) {
            this.value = value;
            return this;
        }

        public Tag build() {
            Tag tag = new Tag();
            tag.date = date;
            tag.id = id;
            tag.user = user;
            tag.value = value;
            return tag;
        }

    }

}
