
package com.bring.global.demo.models;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Bank {

    @Expose
    private String name;
    @SerializedName("national_identifier")
    private String nationalIdentifier;

    public String getName() {
        return name;
    }

    public String getNationalIdentifier() {
        return nationalIdentifier;
    }

    public static class Builder {

        private String name;
        private String nationalIdentifier;

        public Bank.Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Bank.Builder withNationalIdentifier(String nationalIdentifier) {
            this.nationalIdentifier = nationalIdentifier;
            return this;
        }

        public Bank build() {
            Bank bank = new Bank();
            bank.name = name;
            bank.nationalIdentifier = nationalIdentifier;
            return bank;
        }

    }

}
